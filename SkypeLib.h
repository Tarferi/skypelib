// SkypeLib.h

#pragma once

#include "Stdafx.h"

namespace SkypeLib {
	class ChatManager;
	class Connector;
	class Skype;
	class SkypeUser;
	class SkypeLibC;
	class COMMessage;
	enum STATUS;
}

#include "STATUS.h"
#include "ChatManager.h"
#include "Connector.h"
#include "Skype.h"
#include "SkypeUser.h"
#include "COMMessage.h"

namespace SkypeLib {

	void messageReceiver(COMMessage* msg, void* param);

	class SkypeLibC {
	public:
		SkypeLibC(HWND windowInstance) {
			connector = new Connector(windowInstance);
			connector->messageReceiver=messageReceiver;
			connector->cbParam=this;
		}

		Skype* load(unsigned int timeOut=0) {
			this->connector->start();
			return NULL;
		}

		void messageReceived(COMMessage* msg) {
			cout << "Library accepted: " << msg->toString() << "\n";
			delete msg;
		}

	private: 
		Connector* connector;
		
	};


}
