#pragma once

#include "SkypeLib.h"

#include <string>
#include <iostream>



namespace SkypeLib {

	class ChatManager;

	public class SkypeUser {
	public:
		SkypeUser(ChatManager* chatManagerInstance) {
			chatManager=chatManagerInstance;
		}
		/*
		virtual void statusChanged(STATUS newStatus){};
		virtual void displayNameChanged(string newDisplayName){};
		virtual void usernameChanged(string newUsername){};
		*/
	private:
		ChatManager* chatManager;
		std::string username;
		std::string displayName;
		STATUS status;
	};


}