#pragma once

#include "SkypeLib.h"
#include <string>
#include <iostream>


namespace SkypeLib {

	class SkypeUser;
	class Skype;

	class ChatManager {

	public:
		ChatManager(Skype* skypeInstance);
		~ChatManager();
		SkypeUser* getUserByUsername(std::string username);

	private:
		Skype* skype;
		void (*userChangedCallback)(SkypeUser*);
	};
}

