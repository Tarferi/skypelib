#pragma once

#include "SkypeLib.h"

namespace SkypeLib {
	class Skype {

	public:
		Skype(SkypeLibC* lib);

		~Skype();

		ChatManager* getChatManager() {
			return chatManager;
		};


		void (*selfStatusChangeCallback)(Skype*);

	private:
		ChatManager* chatManager;
		SkypeLibC* lib;
	};
}

