#pragma once


#include <string>
#include <iostream>
#include <windows.h>
#include <stdio.h>

#include "COMMessage.h"

#pragma comment(lib, "user32.lib")

using std::cout;

namespace SkypeLib {

	class BaseError {
		protected:
			std::string msg_;
			va_list ap_;
		public:
			BaseError(char *fmt, va_list ap) {
				int desired_length= _vsnprintf(NULL, 0, fmt, ap);
				msg_.resize(desired_length);
				int printedlength= _vsnprintf(&msg_[0], msg_.size(), fmt, ap);
				va_end(ap);
				if (printedlength!=-1)
				msg_.resize(printedlength);
			}

			friend std::ostream& operator << (std::ostream& os, const BaseError& e) {
				return os << e.msg_ << std::endl;
			}
	};

	class SystemError : public BaseError {
		private:
			DWORD dwErrorCode;
			std::string err_;
		public:
		SystemError(char *fmt, ...): BaseError(fmt, (va_start(ap_, fmt), ap_)), dwErrorCode(GetLastError()) {
			char* msgbuf;
			int rc= FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,NULL, dwErrorCode, 0, (LPTSTR) &msgbuf, 0, NULL);
			if (rc) {
				err_= msgbuf;
				LocalFree(msgbuf);
			}
		}

		friend std::ostream& operator << (std::ostream& os, const SystemError& e) {
			os << e.msg_ << ": ";
			if (e.err_.empty())
			os << std::hex << e.dwErrorCode << std::endl; 
			else
			os << e.err_;
			return os;
		}
	};

	enum {
		SKYPECONTROLAPI_ATTACH_SUCCESS=0,
		SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION=1,
		SKYPECONTROLAPI_ATTACH_REFUSED=2,
		SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE=3,
		SKYPECONTROLAPI_ATTACH_API_AVAILABLE= 0x8001,
	};

	class Connector {
	public:

		Connector(HWND windowInstance) {
			hasError=false;
			isAvailable=false;
			theError="";
			SkypeWindowProcedureInit=false;
			window=NULL;
			skypeWindow=NULL;
			a=NULL;
			thread=NULL;
			messageReceiver=NULL;
		}


		void (*messageReceiver)(COMMessage*,void*);
		void* cbParam;

		void processSkypeMessage(char* message) {
			COMMessage* msg=new COMMessage(message);
			if(messageReceiver!=NULL) {
				messageReceiver(msg,cbParam);
			} else {
				cout << "Unhandled message: " << msg->toString() << "\n";
				delete msg;
			}
			sendPing();
		}

		void start() {
			MakeThread();
		}

		~Connector() {
			stopThread();
			if(window!=NULL) {
				DestroyWindow(window);
			}
			if(a!=NULL) {
				UnregisterClass((LPCWSTR)a, NULL);
			}
		}

	private:
		UINT discover;
		UINT attach;
		HWND window;
		HWND skypeWindow;

		bool hasError;
		std::string theError;
		bool isAvailable;
		ATOM a;
		HANDLE thread;

		void sendPing() {
//			 SendMessage(skypeWindow, discover, (WPARAM)hWnd, 0);
		}

		void stopThread() {
			if(thread != NULL) {
				TerminateThread(thread,9);
			}
		}

		void SkypeDiscover(HWND hWnd) {
			skypeWindow = NULL;
			LRESULT res= SendMessage(HWND_BROADCAST, discover, (WPARAM)hWnd, 0);
		}

		void SkypeRegisterMessages() {
			discover = RegisterWindowMessageA((LPCSTR)"SkypeControlAPIDiscover");
			if (discover==0) {
				throw SystemError("RegisterWindowMessage");
			}
			attach = RegisterWindowMessageA((LPCSTR)"SkypeControlAPIAttach");
			if (attach==0) {
				throw SystemError("RegisterWindowMessage");
			}
		}

		static LRESULT CALLBACK _SkypeWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
			Connector* c = (Connector *)GetWindowLong(hWnd,GWLP_USERDATA);
			if (c == NULL) {
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
			return c->SkypeWindowProc(hWnd, msg, wParam, lParam);
		}

		bool SkypeWindowProcedureInit;

		LRESULT CALLBACK SkypeWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
			try {
				if(uMsg == attach) {
					HandleSkypeAttach(lParam, wParam);
				} else if(uMsg == discover) {
					HWND hWndOther= (HWND)wParam;
					if (hWndOther!=hWnd) {
						hasError=true;
						theError="Have invalid attach response WHND";
					}
				} else {
					switch(uMsg) {
						case WM_CREATE:
							if(!SkypeWindowProcedureInit) {
								SkypeWindowProcedureInit=true;
								SkypeRegisterMessages();
								SkypeDiscover(hWnd);
								return DefWindowProc(hWnd, uMsg, wParam, lParam);
							}
						break;

						case WM_DESTROY:
							return 0;
						break;

						case WM_COPYDATA:
							HandleSkypeMessage((HWND)wParam, (COPYDATASTRUCT*)lParam);
						break;

						default:
							return DefWindowProc(hWnd, uMsg, wParam, lParam);
						break;
					}
				}
				return 0;
			} catch (BaseError e) {
				hasError=true;
				theError="BaseError";
				return -1;
			}
			catch (...) {
				hasError=true;
				theError="Unknown exception";
				return -1;
			}
		}

		void HandleSkypeMessage(HWND hWndSkype, COPYDATASTRUCT* cds) {
			if (hWndSkype == skypeWindow) {
				processSkypeMessage((char*)cds->lpData);
			} else {
				cout << "Have message from foreign source\n";
			}
		}

		void HandleSkypeAttach(LPARAM lParam, WPARAM wParam) {
			switch(lParam) {

				case SKYPECONTROLAPI_ATTACH_SUCCESS:
					skypeWindow = (HWND)wParam;
					isAvailable=true;
				break;
				
				case SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION:
					isAvailable=false;
				break;
			
				case SKYPECONTROLAPI_ATTACH_REFUSED:
					isAvailable=false;
					hasError=true;
					theError="Refused to attach";
				break;

				case SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE:
					isAvailable=false;
					hasError=true;
					theError="API unavailable";
				break;

				case SKYPECONTROLAPI_ATTACH_API_AVAILABLE:
					isAvailable= false;
					SkypeDiscover(window);
				break;

				default:
					cout  << "Undefined skype message: " << lParam << "\n";
				break;
			}
		}

		HWND MakeWindow() {
			WNDCLASS wndcls;
			memset(&wndcls, 0, sizeof(WNDCLASS)); 
			wndcls.lpfnWndProc = _SkypeWindowProc;
			wndcls.lpszClassName = (LPCWSTR)"slw";
			a= RegisterClass(&wndcls);
			if (a==0) {
				throw SystemError("RegisterClass");
			}
			HWND hWnd = CreateWindowEx(0, wndcls.lpszClassName, (LPCWSTR)"itsme skype window", 0, 0, 0, 50, 50, (HWND)NULL, (HMENU)NULL, (HINSTANCE)NULL, NULL);
			if (hWnd==NULL) {
				throw SystemError("CreateWindowEx");
			}
			SetWindowLong(hWnd, GWLP_USERDATA, (long)this);
			return hWnd;
		}

		DWORD WINAPI SkypeWindowThread(LPVOID lpParameter) {
			window = MakeWindow();
			SendMessage(window,WM_CREATE,(WPARAM)window,(LPARAM)0);
			MSG msg;
			BOOL bRet;
			while ((bRet= GetMessage(&msg, NULL, 0, 0))!=0) {
				if (bRet==-1) {
					break;
				}
				TranslateMessage(&msg); 
				DispatchMessage(&msg); 
			}
			return 0;
		}

		void MakeThread() {
			thread = CreateThread(NULL, 0,nwThread, this, 0, NULL);
			if (thread==NULL || thread==INVALID_HANDLE_VALUE) {
				throw SystemError("CreateThread");
			}
		}

		static unsigned long WINAPI nwThread(void *ptr)  {
			if (!ptr) {
				return -1;
			}
			((Connector*)ptr)->SkypeWindowThread(NULL);
			return 0;
		}
	};

}