#pragma once

#include <string>
#include <iostream>
#include <windows.h>
#include <stdio.h>

namespace SkypeLib {
	class COMMessage {
	public:
		COMMessage(char* contents) {
			std::string str(contents);
			this->contents=str;
		}

		~COMMessage() {
			
		}

		std::string toString() {
			return contents;
		}

	private:
		std::string contents;
	};

}