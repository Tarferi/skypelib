#pragma once

namespace SkypeLib {
	enum STATUS {
		ONLINE,
		AWAY,
		BUSY,
		INVISIBLE,
		UNAVAILABLE,
		OFFLINE
	};
}

